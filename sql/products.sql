-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 11 Juin 2015 à 16:26
-- Version du serveur :  5.6.24
-- Version de PHP :  5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `shop`
--

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `category` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `rating` decimal(2,1) NOT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`id`, `category`, `name`, `description`, `price`, `picture`, `rating`, `date`) VALUES
(1, 45, 'pierre', 'aaaaaaaaaaaaaa aaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaa aaaaaaaaa', '20.00', 'http://lorempixel.com/400/200/', '1.5', '2015-06-11 16:18:54'),
(2, 45, 'sophie', 'bbbbbb bbbbbbbbbbbb bbbbbbbbb bbbbbbbbbbbbb bbbbbbbbbbb bbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbb bbbbbbbbbbbbbbbbbbbbb', '21.00', 'http://lorempixel.com/400/200/', '5.5', '2015-06-11 16:18:54'),
(3, 45, 'paul', 'ccccccccccc cccccccccccccc ccccccccccccccccc cccccccccccccccc ccccccccccccccccccc cccccccccccccccccccccc cccccccccccccc ', '22.00', 'http://lorempixel.com/400/200/', '6.5', '2015-06-11 16:18:54'),
(4, 45, 'jack', 'ddddddddddddd ddddddddddddd dddddddddddddd ddddddddddddd ddddddddddddd dddddddddddddd ddddddddddddd', '23.00', 'http://lorempixel.com/400/200/', '7.5', '2015-06-11 16:18:54'),
(5, 45, 'jean', 'eeeeeeeeeee eeeeeeeeeee eeeeeeeeeeeeeee eeeeeeeeeeeeee eeeeeeeeeeee eeeeeeeeeeeeeeeeeee eeeeeeeeeeeeeeeeeee', '24.00', 'http://lorempixel.com/400/200/', '2.5', '2015-06-11 16:18:54'),
(6, 50, 'gary', 'fffffffffffff fffffffffffff ffffffffffffffffff ffffffffffffffff fffffffffffffffffff fffffffffffffff', '25.00', 'http://lorempixel.com/400/200/', '7.5', '2015-06-11 16:18:54'),
(7, 50, 'patrice', 'eeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeeeeee eeeeeeeeeeeee eeeeeeeeeeeeee eeeeeeeeeeeeee ', '26.00', 'http://lorempixel.com/400/200/', '9.5', '2015-06-11 16:18:54'),
(8, 50, 'cedric', 'gggggggggg gggggggggggggg ggggggggggggggg ggggggggggggggg ggggggggggggggggg ggggggggggggg gggggggggggggg ', '27.00', 'http://lorempixel.com/400/200/', '3.5', '2015-06-11 16:18:54'),
(9, 50, 'hugo', 'hhhhhhhhhhhhh hhhhhhhhhhhhhhhh hhhhhhhhhhhhhhhhhh hhhhhhhhhhhhhhhhhhhhhh hhhhhhhhhhhhhhhh hhhhhhhhhhhhhh ', '28.00', 'http://lorempixel.com/400/200/', '6.5', '2015-06-11 16:18:54'),
(10, 50, 'viviane', 'iiiiiiiiiiii iiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiiiii iiiiiiiiiiiiiiiiiiiii iiiiiiiiiiiiii', '29.00', 'http://lorempixel.com/400/200/', '8.5', '2015-06-11 16:18:54');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
